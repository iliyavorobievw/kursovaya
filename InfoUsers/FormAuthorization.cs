﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Vorobiev_Kursovaya.InfoUsers
{
    public partial class FormAuthorization : FormDesign
    {
        public FormAuthorization()
        {
            InitializeComponent();
        }

        private void FormAuthorization_Load(object sender, EventArgs e)
        {

        }

        private void ButtonEnter_Click(object sender, EventArgs e)
        {
            UserInfo user = GetUser(LoginTextBox.Text.Trim(), PasswordTextBox.Text.Trim());

            if (user != null)
            {
                if (user.Role == 0)
                {
                    MessageBox.Show("Вы агент!");
                }
                else if (user.Role == 1)
                {
                    MessageBox.Show("Вы администратор!");
                }
                this.Hide();
                new MenuForm().Show(this);
            }
            else
            {
                MessageBox.Show("Неверный логин или пароль!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        public UserInfo GetUser(string login, string pass)
        {
            UserInfo user = new UserInfo();
            try
            {
                using (StreamReader sr = new StreamReader(AppData.usersPath))
                {
                    while (!sr.EndOfStream)
                    {
                        string s = sr.ReadLine();
                        string[] elements = s.Split(new char[] { '|' });
                        if (elements[1] == login && elements[2] == pass)
                        {
                            user.Role = int.Parse(elements[0]);
                            user.Login = login;
                            user.Password = pass;
                            user.name = elements[3];
                            user.lastname = elements[4];
                            user.midname = elements[5];
                            user.phone = elements[6];
                            user.address = elements[7];
                            user.workplace = elements[8];



                            break;
                        }
                    }

                    if (user.Login == "" || user.Login == null)
                    {
                        user = null;
                    }

                    sr.Close();
                }
            }
            catch (Exception)
            {
                user = null;
            }
            AppData.userInfo = user;

            return user;
        }


    }
}
