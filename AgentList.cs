﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Vorobiev_Kursovaya.InfoUsers;

namespace Vorobiev_Kursovaya
{
    public partial class AgentList : FormDesign
    {
        List<UserInfo> Worker;
        public AgentList()
        {

            InitializeComponent();
            List<List<string>> rows = File.ReadAllLines(AppData.usersPath).Select(s => s.Split('|').ToList()).ToList();
            Worker = new List<UserInfo>();

            for (int i = 0; i < rows.Count; i++)
            {
                Worker.Add(new UserInfo()
                {
                    name = rows[i][3],
                    lastname = rows[i][4],
                    midname = rows[i][5],
                    phone = rows[i][6],
                    address = rows[i][7],
                    workplace = rows[i][8]
                }
                );
            }
        }

        private void AgentList_Load(object sender, EventArgs e)
        {



            var path = @"contract.txt";
            if (File.Exists(path))
            {
                Dictionary<string, TypeDeal> types = new Dictionary<string, TypeDeal>();
                Dictionary<string, List<List<string>>> contracts = new Dictionary<string, List<List<string>>>();
                List<List<string>> rows = File.ReadAllLines(path).Select(s => s.Split('|').ToList()).ToList();
                for (int i = 0; i < rows.Count; i++)
                {
                    var row = rows[i];
                    var type = row[6];
                    if (!types.ContainsKey(type))
                    {
                        types.Add(type, new TypeDeal(i));
                    }
                    var FIO = row[7];
                    if (!contracts.ContainsKey(FIO))
                    {
                        contracts.Add(FIO, new List<List<string>>());
                    }
                    contracts[FIO].Add(row);
                }

                types["страхование автотранспорта от угона"].mul = 0.2;
                types["добровольное медицинское страхование"].mul = 0.4;
                types["страхование домашнего имущества"].mul = 0.3;



                try
                {
                    for (int i = 0; i < Worker.Count; i++)
                    {
                        var FIO = Worker[i].name + " " + Worker[i].lastname + " " + Worker[i].midname;
                        if (FIO == AppData.userInfo.name + " " + AppData.userInfo.lastname + " " + AppData.userInfo.midname || AppData.userInfo.Role == 1)
                        {
                            double zp = 0;
                            if (contracts.ContainsKey(FIO))
                            {
                                zp = contracts[FIO].Sum(item => Convert.ToInt32(item[3]) * types[item[6]].mul);
                            }
                            WorkersGridView.Rows.Add(FIO, Worker[i].address, Worker[i].phone, Worker[i].workplace, zp.ToString());
                        }


                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                MessageBox.Show("Файл не найден");
            }
        }

        class TypeDeal
        {
            private readonly int id;
            public double mul;

            public TypeDeal(int id)
            {
                this.id = id;
            }
        }



        private void AgentList_FormClosed(object sender, FormClosedEventArgs e)
        {
            Owner.Show();
        }


    }
}
