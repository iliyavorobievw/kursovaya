﻿namespace Vorobiev_Kursovaya
{
    partial class MenuForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonContract = new System.Windows.Forms.Button();
            this.buttonType = new System.Windows.Forms.Button();
            this.buttonFilial = new System.Windows.Forms.Button();
            this.buttonZp = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonContract
            // 
            this.buttonContract.BackColor = System.Drawing.Color.Chocolate;
            this.buttonContract.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonContract.ForeColor = System.Drawing.Color.Transparent;
            this.buttonContract.Location = new System.Drawing.Point(58, 55);
            this.buttonContract.Margin = new System.Windows.Forms.Padding(5);
            this.buttonContract.Name = "buttonContract";
            this.buttonContract.Size = new System.Drawing.Size(313, 86);
            this.buttonContract.TabIndex = 0;
            this.buttonContract.Text = "Договоры";
            this.buttonContract.UseVisualStyleBackColor = false;
            this.buttonContract.Click += new System.EventHandler(this.ButtonContract_Click);
            // 
            // buttonType
            // 
            this.buttonType.BackColor = System.Drawing.Color.Chocolate;
            this.buttonType.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonType.ForeColor = System.Drawing.Color.Transparent;
            this.buttonType.Location = new System.Drawing.Point(58, 151);
            this.buttonType.Margin = new System.Windows.Forms.Padding(5);
            this.buttonType.Name = "buttonType";
            this.buttonType.Size = new System.Drawing.Size(313, 86);
            this.buttonType.TabIndex = 1;
            this.buttonType.Text = "Вид страхования";
            this.buttonType.UseVisualStyleBackColor = false;
            this.buttonType.Click += new System.EventHandler(this.ButtonType_Click);
            // 
            // buttonFilial
            // 
            this.buttonFilial.BackColor = System.Drawing.Color.Chocolate;
            this.buttonFilial.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonFilial.ForeColor = System.Drawing.Color.Transparent;
            this.buttonFilial.Location = new System.Drawing.Point(58, 246);
            this.buttonFilial.Margin = new System.Windows.Forms.Padding(5);
            this.buttonFilial.Name = "buttonFilial";
            this.buttonFilial.Size = new System.Drawing.Size(313, 86);
            this.buttonFilial.TabIndex = 2;
            this.buttonFilial.Text = "Филиалы";
            this.buttonFilial.UseVisualStyleBackColor = false;
            this.buttonFilial.Click += new System.EventHandler(this.ButtonFilial_Click);
            // 
            // buttonZp
            // 
            this.buttonZp.BackColor = System.Drawing.Color.Chocolate;
            this.buttonZp.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonZp.ForeColor = System.Drawing.Color.Transparent;
            this.buttonZp.Location = new System.Drawing.Point(58, 342);
            this.buttonZp.Margin = new System.Windows.Forms.Padding(5);
            this.buttonZp.Name = "buttonZp";
            this.buttonZp.Size = new System.Drawing.Size(313, 86);
            this.buttonZp.TabIndex = 3;
            this.buttonZp.Text = "Зарплата";
            this.buttonZp.UseVisualStyleBackColor = false;
            this.buttonZp.Click += new System.EventHandler(this.Button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(123, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 25);
            this.label1.TabIndex = 4;
            this.label1.Click += new System.EventHandler(this.Label1_Click);
            // 
            // MenuForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PeachPuff;
            this.ClientSize = new System.Drawing.Size(440, 506);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonZp);
            this.Controls.Add(this.buttonFilial);
            this.Controls.Add(this.buttonType);
            this.Controls.Add(this.buttonContract);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ForeColor = System.Drawing.Color.Transparent;
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "MenuForm";
            this.Text = "Меню";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AuthForm_FormClosed);
            this.Load += new System.EventHandler(this.MenuForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonContract;
        private System.Windows.Forms.Button buttonType;
        private System.Windows.Forms.Button buttonFilial;
        private System.Windows.Forms.Button buttonZp;
        private System.Windows.Forms.Label label1;
    }
}

