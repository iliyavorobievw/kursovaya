﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Vorobiev_Kursovaya.InfoUsers;

namespace Vorobiev_Kursovaya
{
    public partial class MenuForm : FormDesign
    {
        public MenuForm()
        {
            InitializeComponent();
        }



        private void ButtonContract_Click(object sender, EventArgs e)
        {
            this.Hide();
            new FormContract().Show(this);


        }

        private void ButtonType_Click(object sender, EventArgs e)
        {
            this.Hide();
            new FormTypeSave().Show(this);
        }

        private void ButtonFilial_Click(object sender, EventArgs e)
        {
            this.Hide();
            new FormFilial75().Show(this);
        }

        private void AuthForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();

        }

        private void Button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            new AgentList().Show(this);
        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void MenuForm_Load(object sender, EventArgs e)
        {
            if (AppData.userInfo.Role == 0)
            {
                label1.Text = "Вы менеджер";
             

            }
            else if (AppData.userInfo.Role == 1)
            {
                label1.Text = "Вы администратор";
            }



        }
    }
}

