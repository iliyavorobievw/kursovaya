﻿namespace Vorobiev_Kursovaya
{
    partial class FormFilial75
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Filial45 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bhyb4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DelButton = new System.Windows.Forms.DataGridViewButtonColumn();
            this.B_im = new System.Windows.Forms.Button();
            this.B_ex = new System.Windows.Forms.Button();
            this.buttonGoToAgentList = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column3,
            this.Filial45,
            this.bhyb4,
            this.Column1,
            this.Column2,
            this.DelButton});
            this.dataGridView1.Location = new System.Drawing.Point(12, 12);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(742, 176);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.Tag = "Удалить";
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView1_CellContentClick);
            // 
            // Column3
            // 
            this.Column3.Frozen = true;
            this.Column3.HeaderText = "id";
            this.Column3.Name = "Column3";
            this.Column3.Width = 80;
            // 
            // Filial45
            // 
            this.Filial45.HeaderText = "Филиал";
            this.Filial45.Name = "Filial45";
            // 
            // bhyb4
            // 
            this.bhyb4.HeaderText = "Наименование филиала";
            this.bhyb4.Name = "bhyb4";
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Адрес";
            this.Column1.Name = "Column1";
            this.Column1.Width = 140;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Телефон";
            this.Column2.Name = "Column2";
            this.Column2.Width = 120;
            // 
            // DelButton
            // 
            this.DelButton.HeaderText = "Удаление";
            this.DelButton.Name = "DelButton";
            this.DelButton.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.DelButton.Text = "Удалить";
            this.DelButton.UseColumnTextForButtonValue = true;
            // 
            // B_im
            // 
            this.B_im.Location = new System.Drawing.Point(12, 194);
            this.B_im.Name = "B_im";
            this.B_im.Size = new System.Drawing.Size(75, 23);
            this.B_im.TabIndex = 1;
            this.B_im.Text = "Импорт";
            this.B_im.UseVisualStyleBackColor = true;
            this.B_im.Click += new System.EventHandler(this.Button_Im);
            // 
            // B_ex
            // 
            this.B_ex.Location = new System.Drawing.Point(679, 194);
            this.B_ex.Name = "B_ex";
            this.B_ex.Size = new System.Drawing.Size(75, 23);
            this.B_ex.TabIndex = 2;
            this.B_ex.Text = "Экспорт";
            this.B_ex.UseVisualStyleBackColor = true;
            this.B_ex.Click += new System.EventHandler(this.Click_Ex);
            // 
            // buttonGoToAgentList
            // 
            this.buttonGoToAgentList.Location = new System.Drawing.Point(679, 251);
            this.buttonGoToAgentList.Name = "buttonGoToAgentList";
            this.buttonGoToAgentList.Size = new System.Drawing.Size(75, 23);
            this.buttonGoToAgentList.TabIndex = 4;
            this.buttonGoToAgentList.Text = "Зарплата ";
            this.buttonGoToAgentList.UseVisualStyleBackColor = true;
            this.buttonGoToAgentList.Click += new System.EventHandler(this.ButtonGoToAgentList_Click);
            // 
            // FormFilial75
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(765, 290);
            this.Controls.Add(this.buttonGoToAgentList);
            this.Controls.Add(this.B_ex);
            this.Controls.Add(this.B_im);
            this.Controls.Add(this.dataGridView1);
            this.Name = "FormFilial75";
            this.Text = "Филиалы";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormFilial75_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormFilial75_FormClosed);
            this.Load += new System.EventHandler(this.FormFilial_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button B_im;
        private System.Windows.Forms.Button B_ex;
        private System.Windows.Forms.Button buttonGoToAgentList;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Filial45;
        private System.Windows.Forms.DataGridViewTextBoxColumn bhyb4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewButtonColumn DelButton;
    }
}