﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using Vorobiev_Kursovaya.InfoUsers;

namespace Vorobiev_Kursovaya
{
    public partial class FormFilial75 : FormDesign
    {
        public FormFilial75()
        {
            InitializeComponent();
            var path = @"filial.txt";
            if (File.Exists(path))
            {
                ImportDataGrid(path);
            }
            else
            {
                MessageBox.Show("Файл не найден");
            }
        }

        private void DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 5)
            {
                dataGridView1.Rows.Remove(dataGridView1.Rows[e.RowIndex]);
            }
        }
        private void ImportDataGrid(string path)
        {
            try
            {
                dataGridView1.Rows.Clear();
                List<string[]> rows = File.ReadAllLines(path).Select(s => s.Split('|')).ToList();
                foreach (var row in rows)
                {
                    dataGridView1.Rows.Add(row);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ExportDataGrid(string path)
        {
            var rows = new List<List<string>>();
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                List<string> cells = new List<string>();
                foreach (DataGridViewCell cell in row.Cells)
                {
                    cells.Add(cell.FormattedValue.ToString());
                }
                rows.Add(cells);
            }
            if (rows.Count > 0)
            {
                StreamWriter writer = new StreamWriter(path);
                foreach (var row in rows)
                {
                    var rowIsClean = true;
                    foreach (var cell in row)
                    {
                        if (!string.IsNullOrEmpty(cell))
                        {
                            rowIsClean = false;
                            break;
                        }
                    }
                    if (!rowIsClean)
                    {
                        writer.WriteLine(row.Aggregate((l, r) => l + "|" + r));
                    }
                }
                writer.Close();
            }
        }

        private void FormFilial_Load(object sender, EventArgs e)
        {
            
            
          
            if (AppData.userInfo.Role == 0)
            {
                B_ex.Visible = false;
                B_ex.Enabled = false;
                B_im.Visible = false;
                B_ex.Enabled = false;

            }
            else if (AppData.userInfo.Role == 1)
            {
                B_ex.Visible = true;
                B_ex.Enabled = true;
                B_im.Visible = true;
                B_ex.Enabled = true;
            }

            
            //element.visible = false
              //element.enabled= false



        }


        private void Button_Im(object sender, EventArgs e)
        {
            var dialog = new OpenFileDialog
            {
                Title = "Импорт данных",
                DefaultExt = "txt",
                Filter = "Текстовые документы (*.txt)|*.txt|Все файлы (*.*)|*.*"
            };
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                var path = dialog.FileName;
                if (File.Exists(path))
                {
                    ImportDataGrid(path);
                }
                else
                {
                    MessageBox.Show("Файл не найден");
                }
            }
        }

        private void Click_Ex(object sender, EventArgs e)
        {
            var dialog = new SaveFileDialog
            {
                Title = "Экспорт данных",
                DefaultExt = "txt",
                Filter = "Текстовые документы (*.txt)|*.txt|Все файлы (*.*)|*.*"
            };
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                var path = dialog.FileName;
                ExportDataGrid(path);
            }
        }

        private void FormFilial75_FormClosed(object sender, FormClosedEventArgs e)
        {
            Owner.Show();
        }

       

        private void ButtonGoToAgentList_Click(object sender, EventArgs e)
        {
            this.Hide();
            new AgentList().Show(this);
        }

        private void FormFilial75_FormClosing(object sender, FormClosingEventArgs e)
        {
            var path = @"filial.txt"; ExportDataGrid(path);

        }
    }
}
